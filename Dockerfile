FROM ruby:2.5
COPY . /var/www/ruby
WORKDIR /var/www/ruby

RUN bundle install

CMD ["ruby","index.rb"]
